/**
 * Created by urian39 on 17-04-12
 */

fun printSum(a: Int, b: Int): Unit {
    println("$a + $b = ${a + b}");
}

fun main(args: Array<String>) {
    printSum(3,4);
}

/* vim: set ts=4 sw=4 et:*/
