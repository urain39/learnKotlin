fun main(args: Array<String>) {
    var s1 = "Hello World"
    println("$s1")

    s1 = "${s1.replace("Hello","Hi")}"
    println("$s1")
}

/* vim: set ts=4 sw=4 et:*/
