/**
 * Created by urain39 on 17-4-11.
 */

/* <function name>([<function args>:<args type>]) [return type] {...}*/
fun sum(a: Int, b: Int): Int {
    return a + b
}

fun main(args: Array<String>) {
    println("10 + 30 = ${sum(10,30)}")
}

// vim:set ts=4 sw=4 et: