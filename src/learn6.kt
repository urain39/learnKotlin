fun main(args: Array<String>) {
    //var a
    //a += 1 这样用会被报错，必须初始化a

    var a = 1
    a += 1

    //var b
    //b = 3 提示必须有类型注释，或被初始化

    var b = 2
    println("a is $a, b is $b");
}

/* vim: set ts=4 sw=4 et:*/
