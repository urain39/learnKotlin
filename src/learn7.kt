fun main(args: Array<String>) {
    var a  = 1
    val s1 = "a is $a"
    a = 2
    val s2 = "${s1.replace("is","was")}, but now is $a"

    var s3 = "Hello World(1)"
    var s4 = "Hello World(2)"

    println("$s2");
    println("$s1");
    println("$s3");
    println("$s4");
}

/* vim: set ts=4 sw=4 et:*/
